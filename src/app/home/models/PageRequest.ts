export class PageRequest {
  static of(page: number, size: number) {
    return new PageRequest(page.toString(), size.toString());
  }

  static builder() {
    return new PageRequestBuilder();
  }

  constructor(public page: string, public size: string) {}
}

class PageRequestBuilder {
  _page: number;
  _size: number;

  page(page: number) {
    this._page = page;
    return this;
  }

  size(size: number) {
    this._size = size;
    return this;
  }

  build() {
    return new PageRequest(this._page.toString(), this._size.toString());
  }
}
