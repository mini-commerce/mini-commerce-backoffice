import { Component, OnInit, Inject } from '@angular/core';
import { Product } from '@app/home/models/Product';
import { InventoryService } from '@app/home/services/inventory.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent implements OnInit {
  formGroup: FormGroup;
  product: Product = new Product();

  constructor(
    public dialogRef: MatDialogRef<ProductFormComponent>,
    private fb: FormBuilder,
    private inventoryService: InventoryService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    if (this.data.product != null) {
      this.product = this.data.product;
    }
    this.formGroup = this.fb.group({
      id: new FormControl(this.product.id),
      name: new FormControl(this.product.name, Validators.required),
      description: new FormControl(this.product.description, Validators.required),
    });
  }

  save() {
    this.inventoryService.save(this.formGroup.value).subscribe(
      (product: Product) => {
        this.product = product;
        console.log(`Saved product ${product.id} - ${product.name}`);
        this.dialogRef.close();
        this.notifyUser(`Saved product ${product.id} - ${product.name}`);
      },
      (error) => {
        console.log(JSON.stringify(error.error.error));
        this.notifyUser(`Error saving product ${error.error.error}`);
      }
    );
  }

  close() {
    this.dialogRef.close();
  }

  notifyUser(message: string) {
    this._snackBar.open(message, 'close', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
