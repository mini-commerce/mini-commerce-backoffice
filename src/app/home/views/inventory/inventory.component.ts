import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Page } from '../../models/Page';
import { PageRequest } from '../../models/PageRequest';
import { Product } from '../../models/Product';
import { InventoryService } from '../../services/inventory.service';
import { ProductFormComponent } from '../product-form/product-form.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss'],
})
export class InventoryComponent implements OnInit {
  products = new Array<Product>();
  displayedColumns: string[] = ['name', 'description', 'actions'];

  // MatPaginator Inputs
  length = 0;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 100];

  constructor(private inventoryService: InventoryService, private _snackBar: MatSnackBar, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.loadInitialPage();
  }

  loadPage(event: PageEvent) {
    this.loadPageFromServer(PageRequest.builder().page(event.pageIndex).size(event.pageSize).build());
  }

  createDialog(product: Product): void {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      width: '500pt',
      data: { product },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`The dialog was closed with result ${result}`);
      this.loadInitialPage();
    });
  }

  edit(product: Product) {
    console.log(`edit item ${product.id}`);
    this.createDialog(product);
  }

  delete(id: number) {
    console.log(`deleting item ${id}`);
    this.inventoryService.delete(id).subscribe(
      (result: void) => {
        console.log(`deleted product ${id}`);
        this.notifyUser(`deleted product ${id}`);
        this.loadInitialPage();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  notifyUser(message: string) {
    this._snackBar.open(message, 'close', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  private loadInitialPage() {
    this.loadPageFromServer(PageRequest.builder().page(0).size(this.pageSize).build());
  }

  private loadPageFromServer(pageRequest: PageRequest) {
    this.inventoryService.findAll(pageRequest).subscribe(
      (page: Page<Product>) => {
        this.products = page.content;
        this.pageSize = page.size;
        this.length = page.totalElements;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
