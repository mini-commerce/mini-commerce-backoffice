import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PageRequest } from '../models/PageRequest';
import { Product } from '../models/Product';
import { Page } from '../models/Page';

@Injectable({
  providedIn: 'root',
})
export class InventoryService {
  ENDPOINT = 'http://192.168.1.42:8080/catalog/v1/product';

  constructor(private httpClient: HttpClient) {}

  public findAll(pageRequest: PageRequest): Observable<Page<Product>> {
    return this.httpClient.get<Page<Product>>(`${this.ENDPOINT}`, {
      params: { page: pageRequest.page, size: pageRequest.size },
    });
  }

  public save(product: Product): Observable<Product> {
    return this.httpClient.post<Product>(`${this.ENDPOINT}`, product);
  }

  public delete(id: number): Observable<void> {
    console.log(`deleting product ${this.ENDPOINT}/${id}`);
    return this.httpClient.delete<void>(`${this.ENDPOINT}/${id}`);
  }
}
